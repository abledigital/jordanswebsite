<!-- Booking -->
<section class="section-booking bg1-pattern p-t-100 p-b-110">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 p-b-30">
					<div class="t-center">
						<span class="tit2 t-center">
							Reservation
						</span>

						<h3 class="tit3 t-center m-b-35 m-t-2">
							Book A Seat
						</h3>
            <div class='container'>
              <h5><b>VIP Includes (2 tables):</b></h5><br />
              <ul>
			  	  <li>Semi-Private Balcony seating</li>
                  <li>2 Bottles of Champagne (per table)</li>
                  <li>First served for all courses</li>
                  <li>A gift for each person</li>
              </ul>
              <br />
			  <p id='success-message' style='color: green; font-size: 25px;'></p>
			  <p id='error-message' style='color: red; font-size: 25px;'></p>
            </div>
					</div>

					<form class="wrap-form-booking">
						<div class="row">
							<div class="col-md-6">
								<!-- Date -->
								<!-- <span class="txt9">
									Date
								</span>

								<div class="wrap-inputdate pos-relative txt10 size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input class="my-calendar bo-rad-10 sizefull txt10 p-l-20" type="text" name="date">
									<i class="btn-calendar fa fa-calendar ab-r-m hov-pointer m-r-18" aria-hidden="true"></i>
								</div> -->

								<!-- People -->
								<span class="txt9">
									People
								</span>

								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select class="selection-1" name="people">
										<option>1 person</option>
										<!-- <option>2 people</option>
										<option>3 people</option>
										<option>4 people</option> -->
										<!-- <option>5 people</option>
										<option>6 people</option>
										<option>7 people</option>
										<option>8 people</option>
										<option>9 people</option>
										<option>10 people</option>
										<option>11 people</option>
										<option>12 people</option> -->
									</select>
								</div>
							</div>

							<div class="col-md-6">
								<!-- Name -->
								<span class="txt9">
									Full Name
								</span>

								<div class="wrap-inputdate size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input id='fullName' class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="name" placeholder="Name">
								</div>

								<!-- Email -->
								<span class="txt9">
									Email
								</span>

								<div class="wrap-inputdate size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<input id='email' class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="email" placeholder="Email">
								</div>

								<span class="txt9">
									Appetizer
								</span>

								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select id='appetizer' class="selection-1" name="people">
										<option>Crab Mac & Cheese Ball</option>
										<option>Veggie Chili w/ Truffle Honey</option>
									</select>
								</div>
								<span class="txt9">
									Entree
								</span>
								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select id='entree' class="selection-1" name="people">
										<option>Honey Garlic Salmon</option>
										<option>Coconut Sticky Rice - Chicken</option>
										<option>Coconut Sticky Rice - Short Rib</option>
									</select>
								</div>

								<span class="txt9">
									Dessert
								</span>
								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select id='dessert' class="selection-1" name="people">
										<option>Red Velvet Beignet</option>
										<option>Straw-Lemon Shortcake</option>
										
									</select>
								</div>

								<span class="txt9">
									Wine
								</span>
								<div class="wrap-inputpeople size12 bo2 bo-rad-10 m-t-3 m-b-23">
									<!-- Select2 -->
									<select id='wine' class="selection-1" name="people">
										<option>Red</option>
										<option>White</option>
										
									</select>
								</div>
								<!-- VIP -->
								<span class='txt9'>
									VIP?
								</span>
								<div class="size12 m-t-3 m-b-23">
									<input id='vip' class="" type="checkbox" name="vip">
								</div>
								<input id='store_access_token' type='text' hidden>
							</div>
						</div>

						<!-- Disable VIP after 12 purchases -->
						<script>
							function getCookie(cname) {
								var name = cname + "=";
								var decodedCookie = decodeURIComponent(document.cookie);
								var ca = decodedCookie.split(';');
								for(var i = 0; i <ca.length; i++) {
									var c = ca[i];
									while (c.charAt(0) == ' ') {
									c = c.substring(1);
									}
									if (c.indexOf(name) == 0) {
									return c.substring(name.length, c.length);
									}
								}
								return "";
							}

							function getAbleAccessToken () {
								$.ajax({
									url: 'https://able.systems/api/login',
									type: 'POST',
									headers: {
										'Content-Type': 'application/json', 
										'Access-Control-Request-Origin': '*'
										},
									data: JSON.stringify({ aikey: '97983503181807616' }),
									error: function (result) {
										alert('Failed to get access token.\n\n'+ JSON.stringify(result.responseJSON))
									},
									success: function (result) {
										document.cookie = `token=${result.token}`
									}
								})
							}

							function checkOrderAmounts () {
								$.ajax({
									url: 'https://able.systems/api/services/payments/available?ticket=vip',
									type: 'GET',
									headers: {
										
									},
									error: function (result) {
										document.getElementById('error-message').innerHTML = 'Error occurred processing your payment. Please try again later.'
										console.log('Error! Failed to get ticket status from server.\n\n'+ JSON.stringify(result.responseJSON))
									},
									success: function (result) {
										if (result.response === 14) {
											document.getElementById('vip').disabled = true
										} else {
											document.getElementById('vip').disabled = false
										}
									}
								})
							}

							function checkOrderAmountsTwo () {
								$.ajax({
									url: 'https://able.systems/api/services/payments/available?ticket=regular',
									type: 'GET',
									headers: {
										
									},
									error: function (result) {
										document.getElementById('error-message').innerHTML = 'Error occurred processing your payment. Please try again later.'
										console.log('Error! Failed to get ticket status from server.\n\n'+ JSON.stringify(result.responseJSON))
									},
									success: function (result) {
										if (result.response >= 38) {
											$( "#paypal-button-container" ).hide()
											document.getElementById('error-message').innerHTML = 'Tickets Sold Out.'
										} else {
											$( "#paypal-button-container" ).show()
										}
									}
								})
							}

							function updatePaymentInformation (success_id) {
								let fullName = document.getElementById('fullName').value
								let email = document.getElementById('email').value
								let appetizer = document.getElementById('appetizer').value
								let entree = document.getElementById('entree').value
								let dessert = document.getElementById('dessert').value
								let wine = document.getElementById('wine').value

								$.ajax({
									url: 'https://able.systems/api/services/payments/update',
									type: 'PUT',
									headers: {
										
									},
									data: JSON.stringify({ fullName: fullName, email: email, success_id: success_id, appetizer: appetizer, entree: entree, dessert: dessert, wine: wine }),
									error: function (result) {
										document.getElementById('error-message').innerHTML = 'Error occurred processing your payment. Please try again later.'
										console.log('Failed to update payment information.\n\n'+ JSON.stringify(result.responseJSON))
									},
									success: function (result) {
										document.getElementById('success-message').innerHTML = 'Payment was successful! Thank you for your purchase.'
									}
								})
							}
							checkOrderAmounts()
							checkOrderAmountsTwo()
						</script>

						<!-- <div class="wrap-btn-booking flex-c-m m-t-6">
							<button type="submit" class="btn3 flex-c-m size13 txt11 trans-0-4">
								Book Table
							</button>
						</div> -->
                        <div id="paypal-button-container"></div>
						<script>
							// Render the PayPal button into #paypal-button-container
							paypal.Buttons({
								style: {
									color:  'gold',
									shape:  'pill',
									label:  'pay',
									height: 40
								},

								// Set up the transaction
								createOrder: function(data, actions) {
									return actions.order.create({
										purchase_units: [{
											amount: {
												value: document.getElementById('vip').checked === true ? '150.00' : '100.00' 
											}
										}]
									});
								},

								// Finalize the transaction
								onApprove: function(data, actions) {
									return actions.order.capture().then(function(details) {
										console.log('Transaction completed by ' + details.payer.name.given_name)
										document.getElementById('success-message').innerHTML = 'Payment was successful! Thank you for your purchase.'
										// Show a success message to the buyer
										let fullName = document.getElementById('fullName').value
										let email = document.getElementById('email').value
										let appetizer = document.getElementById('appetizer').value
										let entree = document.getElementById('entree').value
										let dessert = document.getElementById('dessert').value
										let wine = document.getElementById('wine').value
										let vip = document.getElementById('vip').checked === true ? 'vip' : 'regular'
										let order_data = {'fullName': fullName, 'email': email, 'appetizer': appetizer, 'entree': entree,
										'dessert': dessert, 'wine': wine, 'ticket_type': vip}
										$.post({
											url: 'https://able.systems/api/services/payments/confirm',
											data: JSON.stringify({ orderID: details.id, vendor: 'bosschefjordii', order_data: order_data }),
											error: function (result) {
												console.log(result)
												// document.getElementById('error-message').innerHTML = 'Error occurred processing your payment. Please try again later.'
												// console.log('Failed to confirm payment.\n\n'+ result.responseJSON)
											},
											success: function (result) {
												// updatePaymentInformation(result.success_id)
											}
										});
									});
								}
							}).render('#paypal-button-container');
						</script>
					</form>
				</div>

				<div class="col-lg-6 p-b-30 p-t-18">
					<div class="wrap-pic-booking size2 bo-rad-10 hov-img-zoom m-l-r-auto">
						<img src="https://i.ibb.co/qsDNcfG/IMG-1758.jpg" alt="A Popup Palate Experience">
            <i>*All menu selections are <b>final</b> and there will be no alterations to menu items*</i>
					</div>
				</div>
			</div>
		</div>
	</section>

<!DOCTYPE html>
<html lang="en">
    <head>
            <title>A Pop Up Palate Experience</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!--===============================================================================================-->
            <link rel="icon" type="image/png" href="images/icons/favicon.png"/>
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="fonts/themify/themify-icons.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/css-hamburgers/hamburgers.min.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/animsition/css/animsition.min.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/daterangepicker/daterangepicker.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/slick/slick.css">
        <!--===============================================================================================-->
            <link rel="stylesheet" type="text/css" href="vendor/lightbox2/css/lightbox.min.css">
        <!--===============================================================================================-->
            <link rel="apple-touch-icon" sizes="57x57" href="images/icon/apple-icon-57x57.png">
            <link rel="apple-touch-icon" sizes="60x60" href="images/icon/apple-icon-60x60.png">
            <link rel="apple-touch-icon" sizes="72x72" href="images/icon/apple-icon-72x72.png">
            <link rel="apple-touch-icon" sizes="76x76" href="images/icon/apple-icon-76x76.png">
            <link rel="apple-touch-icon" sizes="114x114" href="images/icon/apple-icon-114x114.png">
            <link rel="apple-touch-icon" sizes="120x120" href="images/icon/apple-icon-120x120.png">
            <link rel="apple-touch-icon" sizes="144x144" href="images/icon/apple-icon-144x144.png">
            <link rel="apple-touch-icon" sizes="152x152" href="images/icon/apple-icon-152x152.png">
            <link rel="apple-touch-icon" sizes="180x180" href="images/icon/apple-icon-180x180.png">
            <link rel="icon" type="image/png" sizes="192x192"  href="images/icon/android-icon-192x192.png">
            <link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon-32x32.png">
            <link rel="icon" type="image/png" sizes="96x96" href="images/icon/favicon-96x96.png">
            <link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon-16x16.png">
            <link rel="manifest" href="/manifest.json">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="images/icon/ms-icon-144x144.png">
            <meta name="theme-color" content="#ffffff">
            <link rel="stylesheet" type="text/css" href="css/util.css">
            <link rel="stylesheet" type="text/css" href="css/main.css">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

            <script src="https://www.paypal.com/sdk/js?client-id=AZlRUHkjQBI5Xwkq9t_TNPxEUJ_4eeb3oge0JKnYyPXRL1dzBU7vY5HJhfyfi1GXR8tETDGg7KXjmjBG&currency=USD"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
        <!--===============================================================================================-->
        </head>
        <body class="animsition">
            <!-- Header -->
            <header>
                <!-- Header desktop -->
                <div class="wrap-menu-header gradient1 trans-0-4">
                    <div class="container h-full">
                        <div class="wrap_header trans-0-3">
                            <!-- Logo -->
                            <div class="logo">
                                <a href="index.html">
                                    <img src="images/assets/transparent_chefjordii.PNG" alt="BossChefJordii" data-logofixed="images/assets/transparent_chefjordii.PNG">
                                </a>
                            </div>
        
                            <!-- Menu -->
                            <div class="wrap_menu p-l-45 p-l-0-xl">
                                <nav class="menu">
                                    <ul class="main_menu">
                                        <li>
                                            <a href="index.php">Home</a>
                                        </li>
        
                                        <li>
                                            <a href="menu.php">Menu</a>
                                        </li>
        
                                        <li>
                                            <a href="reservation.php">Reservation</a>
                                        </li>
        
                                        <!-- <li>
                                            <a href="gallery.php">Gallery</a>
                                        </li> -->
        
                                        <!-- <li>
                                            <a href="about.html">About</a>
                                        </li>
        
                                        <li>
                                            <a href="blog.html">Blog</a>
                                        </li> -->
        
                                        <!-- <li>
                                            <a href="contact.html">Contact</a>
                                        </li> -->
                                    </ul>
                                </nav>
                            </div>
        
                            <!-- Social -->
                            <div class="social flex-w flex-l-m p-r-20">
                                <a href="https://www.instagram.com/bosschef_jordii/"><i class="fa fa-instagram m-l-21" aria-hidden="true"></i></a>
                                <a href="https://www.facebook.com/jordii.mills"><i class="fa fa-facebook m-l-21" aria-hidden="true"></i></a>
                                <a href="mailto:bosschefjordii@gmail.com"><i class="fa fa-envelope m-l-18" aria-hidden="true"></i></a>
                                <!-- <a href="#"><i class="fa fa-twitter m-l-21" aria-hidden="true"></i></a> -->
        
                                <button class="btn-show-sidebar m-l-33 trans-0-4"></button>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
        
            <!-- Sidebar -->
            <aside class="sidebar trans-0-4">
                <!-- Button Hide sidebar -->
                <button class="btn-hide-sidebar ti-close color0-hov trans-0-4"></button>
        
                <!-- - -->
                <ul class="menu-sidebar p-t-95 p-b-70">
                    <li class="t-center m-b-13">
                        <a href="index.php" class="txt19">Home</a>
                    </li>
        
                    <li class="t-center m-b-13">
                        <a href="menu.php" class="txt19">Menu</a>
                    </li>
        
                    <!-- <li class="t-center m-b-13">
                        <a href="gallery.php" class="txt19">Gallery</a>
                    </li> -->
        
                    <!-- <li class="t-center m-b-13">
                        <a href="about.html" class="txt19">About</a>
                    </li>
        
                    <li class="t-center m-b-13">
                        <a href="blog.html" class="txt19">Blog</a>
                    </li> -->
        
                    <!-- <li class="t-center m-b-33">
                        <a href="contact.html" class="txt19">Contact</a>
                    </li> -->
        
                    <li class="t-center">
                        <!-- Button3 -->
                        <a href="reservation.php" class="btn3 flex-c-m size13 txt11 trans-0-4 m-l-r-auto">
                            Reservation
                        </a>
                    </li>
                </ul>
        
                <!-- - -->
                <div class="gallery-sidebar t-center p-l-60 p-r-60 p-b-40">
                    <!-- - -->
                    <h4 class="txt20 m-b-33">
                        Gallery
                    </h4>
        
                    <!-- Gallery -->
                    <div class="wrap-gallery-sidebar flex-w">
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/4d7713ddade39e89f00483c1d56ec2b8/5DAA1D8D/t51.2885-15/e35/p1080x1080/56905174_567868573735697_5336691555257718129_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img id="dishOne" src="https://instagram.fbos1-1.fna.fbcdn.net/vp/4d7713ddade39e89f00483c1d56ec2b8/5DAA1D8D/t51.2885-15/e35/p1080x1080/56905174_567868573735697_5336691555257718129_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/0c77e7eaf8f3c8446ed9376523096739/5DBA4A95/t51.2885-15/e35/s1080x1080/57506386_193111638329664_4805646072081794383_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/0c77e7eaf8f3c8446ed9376523096739/5DBA4A95/t51.2885-15/e35/s1080x1080/57506386_193111638329664_4805646072081794383_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/24072d37a11305347d4ac1d514f1d173/5DB338BC/t51.2885-15/e35/s1080x1080/57029210_342666389979997_400669934217516021_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/24072d37a11305347d4ac1d514f1d173/5DB338BC/t51.2885-15/e35/s1080x1080/57029210_342666389979997_400669934217516021_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/bcb5b5941cee886a14e330e5bb1b1943/5DB41123/t51.2885-15/e35/s1080x1080/56281750_616181375513194_4729587570388718600_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/bcb5b5941cee886a14e330e5bb1b1943/5DB41123/t51.2885-15/e35/s1080x1080/56281750_616181375513194_4729587570388718600_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/e344cb125cec95bfefa5551cdbc62061/5D9FF314/t51.2885-15/sh0.08/e35/s750x750/56765766_1197206537108317_7075222612656027419_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/e344cb125cec95bfefa5551cdbc62061/5D9FF314/t51.2885-15/sh0.08/e35/s750x750/56765766_1197206537108317_7075222612656027419_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/63c033e272e11fb5a327cc44e69f30ae/5DBAAC52/t51.2885-15/e35/s1080x1080/56483276_188473658802242_3379752096504906211_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/63c033e272e11fb5a327cc44e69f30ae/5DBAAC52/t51.2885-15/e35/s1080x1080/56483276_188473658802242_3379752096504906211_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/6797234d98aef0b3a074081dc55be8c8/5DB047D5/t51.2885-15/e35/54199202_671682396620909_2319760694933142866_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/6797234d98aef0b3a074081dc55be8c8/5DB047D5/t51.2885-15/e35/54199202_671682396620909_2319760694933142866_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/5764517821c8c7eb8b609d33d9089e13/5DC0C612/t51.2885-15/e35/52920088_329233637704143_7098881944963636379_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/5764517821c8c7eb8b609d33d9089e13/5DC0C612/t51.2885-15/e35/52920088_329233637704143_7098881944963636379_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/4a08705d4037668ffb7cd80b97a9c266/5DA9EFCA/t51.2885-15/e35/52803977_2369540856412896_3627596156892440856_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/4a08705d4037668ffb7cd80b97a9c266/5DA9EFCA/t51.2885-15/e35/52803977_2369540856412896_3627596156892440856_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/b6d52632fbd14d4b048713d61722a679/5DB68B21/t51.2885-15/e35/53028759_304593423552241_1147174911595874881_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/b6d52632fbd14d4b048713d61722a679/5DB68B21/t51.2885-15/e35/53028759_304593423552241_1147174911595874881_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/20b2d311d72cae90ce5f9e47835a82ee/5DBCD0BD/t51.2885-15/e35/45725484_362938487786756_5070145445590835859_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/20b2d311d72cae90ce5f9e47835a82ee/5DBCD0BD/t51.2885-15/e35/45725484_362938487786756_5070145445590835859_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
        
                        <a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/61bdb1ccc65d6036227fdbd7de671a6d/5DC5051E/t51.2885-15/e35/12523601_876603709118808_1078124865_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
                            <img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/61bdb1ccc65d6036227fdbd7de671a6d/5DC5051E/t51.2885-15/e35/12523601_876603709118808_1078124865_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
                        </a>
                    </div>
                </div>
            </aside>

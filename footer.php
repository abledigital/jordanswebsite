<!-- Sign up -->
<div class="section-signup bg1-pattern p-t-85 p-b-85">
		<form class="flex-c-m flex-w flex-col-c-m-lg p-l-5 p-r-5">
			<span class="txt5 m-10">
				Specials Sign up
			</span>

			<div class="wrap-input-signup size17 bo2 bo-rad-10 bgwhite pos-relative txt10 m-10">
				<input class="bo-rad-10 sizefull txt10 p-l-20" type="text" name="email-address" placeholder="Email Adrress">
				<i class="fa fa-envelope ab-r-m m-r-18" aria-hidden="true"></i>
			</div>

			<!-- Button3 -->
			<button type="submit" class="btn3 flex-c-m size18 txt11 trans-0-4 m-10">
				Sign-up
			</button>
		</form>
	</div>

<footer class="bg1">
		<div class="container p-t-40 p-b-70">
			<div class="row">
				<div class="col-sm-6 col-md-4 p-t-50">
					<!-- - -->
					<h4 class="txt13 m-b-33">
						Contact Us
					</h4>

					<ul class="m-b-70">
						<!-- <li class="txt14 m-b-14">
							<i class="fa fa-map-marker fs-16 dis-inline-block size19" aria-hidden="true"></i>
							8th floor, 379 Hudson St, New York, NY 10018
						</li>

						<li class="txt14 m-b-14">
							<i class="fa fa-phone fs-16 dis-inline-block size19" aria-hidden="true"></i>
							(+1) 96 716 6879
						</li> -->

						<li class="txt14 m-b-14">
							<i class="fa fa-envelope fs-13 dis-inline-block size19" aria-hidden="true"></i>
							bosschefjordii@gmail.com
						</li>
					</ul>

					<!-- - -->
					<h4 class="txt13 m-b-32">
						Opening Times
					</h4>

					<ul>
						<li class="txt14">
							Based on event times
							<!-- 09:30 AM – 11:00 PM -->
						</li>

						<li class="txt14">
							<!-- Every Day -->
						</li>
					</ul>
				</div>

				<!-- <div class="col-sm-6 col-md-4 p-t-50"> -->
					<!-- - -->
					<!-- <h4 class="txt13 m-b-33">
						Latest twitter
					</h4>

					<div class="m-b-25">
						<span class="fs-13 color2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</span>
						<a href="#" class="txt15">
							@colorlib
						</a>

						<p class="txt14 m-b-18">
							Activello is a good option. It has a slider built into that displays the featured image in the slider.
							<a href="#" class="txt15">
								https://buff.ly/2zaSfAQ
							</a>
						</p>

						<span class="txt16">
							21 Dec 2017
						</span>
					</div>

					<div>
						<span class="fs-13 color2 m-r-5">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</span>
						<a href="#" class="txt15">
							@colorlib
						</a>

						<p class="txt14 m-b-18">
							Activello is a good option. It has a slider built into that displays
							<a href="#" class="txt15">
								https://buff.ly/2zaSfAQ
							</a>
						</p>

						<span class="txt16">
							21 Dec 2017
						</span>
					</div>
				</div> -->

				<div class="col-sm-6 col-md-4 p-t-50">
					<!-- - -->
					<h4 class="txt13 m-b-38">
						Gallery
					</h4>

					<!-- Gallery footer -->
					<div class="wrap-gallery-footer flex-w">
						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/4d7713ddade39e89f00483c1d56ec2b8/5DAA1D8D/t51.2885-15/e35/p1080x1080/56905174_567868573735697_5336691555257718129_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img id="dishOne" src="https://instagram.fbos1-1.fna.fbcdn.net/vp/4d7713ddade39e89f00483c1d56ec2b8/5DAA1D8D/t51.2885-15/e35/p1080x1080/56905174_567868573735697_5336691555257718129_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/0c77e7eaf8f3c8446ed9376523096739/5DBA4A95/t51.2885-15/e35/s1080x1080/57506386_193111638329664_4805646072081794383_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/0c77e7eaf8f3c8446ed9376523096739/5DBA4A95/t51.2885-15/e35/s1080x1080/57506386_193111638329664_4805646072081794383_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/24072d37a11305347d4ac1d514f1d173/5DB338BC/t51.2885-15/e35/s1080x1080/57029210_342666389979997_400669934217516021_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/24072d37a11305347d4ac1d514f1d173/5DB338BC/t51.2885-15/e35/s1080x1080/57029210_342666389979997_400669934217516021_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/bcb5b5941cee886a14e330e5bb1b1943/5DB41123/t51.2885-15/e35/s1080x1080/56281750_616181375513194_4729587570388718600_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/bcb5b5941cee886a14e330e5bb1b1943/5DB41123/t51.2885-15/e35/s1080x1080/56281750_616181375513194_4729587570388718600_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/e344cb125cec95bfefa5551cdbc62061/5D9FF314/t51.2885-15/sh0.08/e35/s750x750/56765766_1197206537108317_7075222612656027419_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/e344cb125cec95bfefa5551cdbc62061/5D9FF314/t51.2885-15/sh0.08/e35/s750x750/56765766_1197206537108317_7075222612656027419_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/63c033e272e11fb5a327cc44e69f30ae/5DBAAC52/t51.2885-15/e35/s1080x1080/56483276_188473658802242_3379752096504906211_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/63c033e272e11fb5a327cc44e69f30ae/5DBAAC52/t51.2885-15/e35/s1080x1080/56483276_188473658802242_3379752096504906211_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/6797234d98aef0b3a074081dc55be8c8/5DB047D5/t51.2885-15/e35/54199202_671682396620909_2319760694933142866_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/6797234d98aef0b3a074081dc55be8c8/5DB047D5/t51.2885-15/e35/54199202_671682396620909_2319760694933142866_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/5764517821c8c7eb8b609d33d9089e13/5DC0C612/t51.2885-15/e35/52920088_329233637704143_7098881944963636379_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/5764517821c8c7eb8b609d33d9089e13/5DC0C612/t51.2885-15/e35/52920088_329233637704143_7098881944963636379_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/4a08705d4037668ffb7cd80b97a9c266/5DA9EFCA/t51.2885-15/e35/52803977_2369540856412896_3627596156892440856_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/4a08705d4037668ffb7cd80b97a9c266/5DA9EFCA/t51.2885-15/e35/52803977_2369540856412896_3627596156892440856_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/b6d52632fbd14d4b048713d61722a679/5DB68B21/t51.2885-15/e35/53028759_304593423552241_1147174911595874881_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/b6d52632fbd14d4b048713d61722a679/5DB68B21/t51.2885-15/e35/53028759_304593423552241_1147174911595874881_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/20b2d311d72cae90ce5f9e47835a82ee/5DBCD0BD/t51.2885-15/e35/45725484_362938487786756_5070145445590835859_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/20b2d311d72cae90ce5f9e47835a82ee/5DBCD0BD/t51.2885-15/e35/45725484_362938487786756_5070145445590835859_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>

						<a class="item-gallery-footer wrap-pic-w" href="https://instagram.fbos1-1.fna.fbcdn.net/vp/61bdb1ccc65d6036227fdbd7de671a6d/5DC5051E/t51.2885-15/e35/12523601_876603709118808_1078124865_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" data-lightbox="gallery-footer">
							<img src="https://instagram.fbos1-1.fna.fbcdn.net/vp/61bdb1ccc65d6036227fdbd7de671a6d/5DC5051E/t51.2885-15/e35/12523601_876603709118808_1078124865_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net" alt="GALLERY">
						</a>
					</div>

				</div>
			</div>
		</div>

		<div class="end-footer bg2">
			<div class="container">
				<div class="flex-sb-m flex-w p-t-22 p-b-22">
					<div class="p-t-5 p-b-5">
                        <a href="https://www.facebook.com/jordii.mills"><i class="fa fa-facebook m-l-21" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/bosschef_jordii/"><i class="fa fa-instagram m-l-21" aria-hidden="true"></i></a>
						<a href="mailto:bosschefjordii@gmail.com"><i class="fa fa-envelope m-l-18" aria-hidden="true"></i></a>
					</div>

					<div class="txt17 p-r-20 p-t-5 p-b-5">
						<p>&copy; BossChefJordii: A Pop Up Palate Experience. All rights reserved. Written by: <a href="https://instagram.com/kopeiam">Kope</a>.</p>
					</div>
				</div>
			</div>
		</div>
	</footer>


	<!-- Back to top -->
	<div class="btn-back-to-top bg0-hov" id="myBtn">
		<span class="symbol-btn-back-to-top">
			<i class="fa fa-angle-double-up" aria-hidden="true"></i>
		</span>
	</div>

	<!-- Container Selection1 -->
	<div id="dropDownSelect1"></div>
	</div>

	<!-- Modal Video 01-->
	<!-- <div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">

		<div class="modal-dialog" role="document" data-dismiss="modal">
			<div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>

			<div class="wrap-video-mo-01">
				<div class="w-full wrap-pic-w op-0-0"><img src="images/icons/video-16-9.jpg" alt="IMG"></div>
				<div class="video-mo-01">
					<iframe src="https://www.youtube.com/embed/5k1hSu2gdKE?rel=0&amp;showinfo=0" allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div> -->



<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/bootstrap/js/popper.js"></script>
	<script type="text/javascript" src="vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/daterangepicker/moment.min.js"></script>
	<script type="text/javascript" src="vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/slick/slick.min.js"></script>
	<script type="text/javascript" src="js/slick-custom.js"></script>
	<!-- <script type="text/javascript" src="js/assets.js"></script> -->
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/parallax100/parallax100.js"></script>
	<script type="text/javascript">
        $('.parallax100').parallax100();
	</script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script type="text/javascript" src="vendor/lightbox2/js/lightbox.min.js"></script>
<!--===============================================================================================-->
	<script src="js/main.js"></script>
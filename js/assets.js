var data = {
    "dishes": {
        "photo1": "https://instagram.fbos1-1.fna.fbcdn.net/vp/38114d6bbe026efd566bacdba4eb4144/5D185721/t51.2885-15/e35/53028759_304593423552241_1147174911595874881_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net",
        "photo2": "https://instagram.fbos1-1.fna.fbcdn.net/vp/6824e673a412666724501c849058d2c8/5D0BBBCA/t51.2885-15/e35/52803977_2369540856412896_3627596156892440856_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net",
        "photo3": "https://instagram.fbos1-1.fna.fbcdn.net/vp/c938a5ba7e13ef33ceb1bdb48b120034/5D26D11E/t51.2885-15/e35/12523601_876603709118808_1078124865_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net",
        "photo4": "https://instagram.fbos1-1.fna.fbcdn.net/vp/d886bb2c2373d89714a236efa1e58052/5D23EB47/t51.2885-15/e15/10665610_1483993661880513_1427941306_n.jpg?_nc_ht=instagram.fbos1-1.fna.fbcdn.net",
        "cover": "https://i.ibb.co/Lh7VTQx/IMG-1650.jpg",
        "flyer": "https://i.ibb.co/zhQkX7R/IMG-1651.jpg",
        "fb_url": "https://www.facebook.com/jordii.mills",
        "ig_url": "https://www.instagram.com/bosschef_jordiii/",
        "rsvp_url": "file:///Users/jdougl24/Documents/Personal/sites/jordans_site/rsvp.html"
    }
}

// set photos programmatically
document.getElementById('dishOne').src = data.dishes.photo1
document.getElementById('photo2').src = data.dishes.photo2
document.getElementById('photo3').src = data.dishes.photo3
document.getElementById('photo4').src = data.dishes.photo4

// set photo links programmatically
document.getElementById('photoLink1').href = data.dishes.photo1
document.getElementById('photoLink2').href = data.dishes.photo2
document.getElementById('photoLink3').href = data.dishes.photo3
document.getElementById('photoLink4').href = data.dishes.photo4

// set cover photo programmatically
document.getElementById('cover-photo').src = data.dishes.cover
document.getElementById('flyer-photo').src = data.dishes.flyer
document.getElementById('flyer-photo-link').href = data.dishes.flyer

// set social media urls
document.getElementById('facebook-url').href = data.dishes.fb_url
document.getElementById('instagram-url').href = data.dishes.ig_url

// nav bar
document.getElementById('home-nav').href = data.dishes.rsvp_url